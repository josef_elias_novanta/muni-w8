//
// Copyright (c) 2018 Novanta Ceska Republika, spol. s r.o.
// This code is proprietary information of Novanta Ceska Republika, spol. s r.o.
// All Rights Reserved. Confidential. Do not copy.
//
// Modification history: 
// Josef Elias, August    12, 2019, Initial release
//

#include "switch.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef  __SWITCH

uint32_t get_switch(uint32_t base){
  uint32_t temp = IORD(base,0);
  return temp;
}

#endif /* __SWITCH */