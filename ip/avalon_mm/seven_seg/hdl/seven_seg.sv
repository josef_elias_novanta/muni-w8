//
// Copyright (c) 2021 Novanta Ceska Republika, spol. s r.o.
// This code is proprietary information of Novanta Ceska Republika, spol. s r.o.
// Copyrights under CC-BY-ND-SA for MUNI PV200 class 2021 
//

module seven_seg(
    input               clock,
    input               reset,
    input       [ 1: 0] avalon_address,
    input               avalon_read, 
    input               avalon_write,
    output reg  [31: 0] avalon_readdata,
    output reg  [31: 0] avalon_readdatavalid,
    input       [31: 0] avalon_writedata,
    output      [ 6: 0] seven_segment_out
);

logic [31: 0] temp;


always @(posedge clock or posedge reset) begin
    if (reset == 1) begin
        avalon_readdatavalid <= 0;
        seven_segment_out <= 0;
    end else begin
        avalon_readdatavalid <= avalon_read;
        
        if (avalon_write == 1) begin
            case (avalon_address)
                0 : seven_segment_out          <= avalon_writedata[6:0];
            endcase
        end

        if (avalon_read == 1) begin
        
            case (avalon_address)
                0 : avalon_readdata <= seven_segment_out;
            endcase
        end
    end
end

endmodule