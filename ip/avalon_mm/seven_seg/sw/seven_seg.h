//
// Copyright (c) 2021 Novanta Ceska Republika, spol. s r.o.
// This code is proprietary information of Novanta Ceska Republika, spol. s r.o.
// Copyrights under CC-BY-ND-SA for MUNI PV200 class 2021 
//

#ifndef SEVEN_SEG_H_
#define SEVEN_SEG_H_

#include "system.h"
#include "io.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint32_t get_seven_seg(uint32_t base);

void set_seven_seg(uint32_t base, uint32_t val);

#ifdef __cplusplus
}
#endif

#endif /* SEVEN_SEG_H_ */

