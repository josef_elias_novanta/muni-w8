//
// Copyright (c) 2021 Novanta Ceska Republika, spol. s r.o.
// This code is proprietary information of Novanta Ceska Republika, spol. s r.o.
// Copyrights under CC-BY-ND-SA for MUNI PV200 class 2021 
//

#include "seven_seg.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef  __SEVEN_SEG

uint32_t get_seven_seg(uint32_t base)
{
  uint32_t temp = IORD(base,0);
  return temp;
}

void set_seven_seg(uint32_t base, uint32_t val)
{
    IOWR(base,0, val);
}

#endif /* __SEVEN_SEG */