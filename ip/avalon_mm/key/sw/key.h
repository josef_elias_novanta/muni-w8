//
// Copyright (c) 2018 Novanta Ceska Republika, spol. s r.o.
// This code is proprietary information of Novanta Ceska Republika, spol. s r.o.
// All Rights Reserved. Confidential. Do not copy.
//
// Modification history: 
// Josef Elias, August   09, 2019, Initial release
//

#ifndef KEY_H_
#define KEY_H_

#include "system.h"
#include "io.h"
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint32_t get_key(uint32_t base);

#ifdef __cplusplus
}
#endif

#endif /* KEY_H_ */
