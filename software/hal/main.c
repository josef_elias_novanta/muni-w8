#include "nios2.h"
#include <stdio.h>
#include "alt_types.h"
#include "io.h"
#include "system.h"
#include "key.h"
#include "seven_seg.h"
#include "switch.h"

int main()
{
    while (1) {
        printf("Hello world\n");
        printf("KEY: %04lx\n", get_key(KEY_0_BASE));
        uint32_t switches = get_switch(SWITCH_0_BASE);
        printf("SWITCH: %04lx\n", switches);
        set_seven_seg(SEVEN_SEG_0_BASE, switches);
        printf("SWITCH: %04lx\n", get_seven_seg(SEVEN_SEG_0_BASE));
        uint32_t temp = getchar();
    }
    return 0;
}
